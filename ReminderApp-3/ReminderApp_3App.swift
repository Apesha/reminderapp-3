//
//  ReminderApp_3App.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/27/20.
//

import SwiftUI

@main
struct ReminderApp_3App: App {
    var body: some Scene {
        WindowGroup {
            MainTabView()
        }
    }
}
