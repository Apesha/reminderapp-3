//
//  ContentView.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/28/20.
//

import SwiftUI

/*
 Making sure that we treat ClassB objects as owned by a ClassA object. so, we don't make changes to ClassB objects without telling the ClassA object to make those changes for us. ClassA needs a method to do that.
 */
class ClassA: ObservableObject {
    @Published var bItems = [ClassB]()
    
    func appendNewB(title: String) {
        bItems.append(ClassB(title: title))
    }
    
    func appendDouble(to someBitem: ClassB) {
        objectWillChange.send() // lets View 1 know about the change in View 2
        someBitem.appendNewDouble()
    }
}

/*
 There is no need for an objectWillChange.send() call in appendNewB, since the doubleItems array will change and publish that change without explicitly doing it ourself. the ClassB code will similarly remove the explicit objectWillChange.send() call.
 */
class ClassB: ObservableObject, Identifiable {
    var id = UUID()
    @Published var title = ""
    @Published var doubleItems = [Double]()
    var x = 0.0
    
    func appendNewDouble() {
        x = x + 1
        doubleItems.append(x)
    }
    
    init(title: String) {
        self.title = title
    }
}

struct ContentView: View {
    @ObservedObject var classAObject = ClassA()
    
    var body: some View {
        NavigationView {
            VStack {
                Button("Append a new ClassB to classA object") {
                    classAObject.appendNewB(title: Date().description)
                }
                List {
                    ForEach(classAObject.bItems) { bItem in
                        NavigationLink (
                            destination: View2(classAObject: self.classAObject, classBObject: bItem))
                        {
                            HStack{
                                Text(bItem.title)
                                Text("  \(bItem.doubleItems.count) doubles")
                            }
                        }
                    }
                }
            }
            .navigationBarTitle(Text("View 1"))
        }
    }
}

/*
 View2 code must now accept the ClassA object (so it knows there is a change to something it owns) AND make the ClassB object an @ObservedObject so the view will redraw when the doubles array is changed.
 */
struct View2: View {
    var classAObject: ClassA
    @ObservedObject var classBObject: ClassB
    
    var body: some View {
        VStack {
            Button("Append a new double") {
                self.classAObject.appendDouble(to: self.classBObject)
            }
            List {
                ForEach(classBObject.doubleItems, id:\.self) { double in
                    Text(String(double))
                }
            }
        }
        .navigationBarTitle(Text(classBObject.title), displayMode: .inline)
    }
}

/*
 Two classes, class A and class B.
 Class A holds an array of type class B.
 Class B holds a string, and array of type Double.
 
 Two views.
 View 1 displays a button which when clicked, appends class B to the array in class A.
 View 2 displays a button whcih when clicked, appends a new Double (0.0) to the array in class B. The view also has a ForEach, displaying text for each double in the array in class B.
 */
