//
//  MainTabView.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/28/20.
//

import SwiftUI

struct MainTabView: View {
    var body: some View {
        TabView {
            CategoryListView()
                .tabItem {
                    Image(systemName: "folder.fill")
                    Text("File System")
                }
            ContentView()
                .tabItem {
                    Image(systemName: "rectangle.and.text.magnifyingglass")
                    Text("Sample Tab")
                }
        }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
