//
//  TaskListView.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/29/20.
//

import SwiftUI
import Combine

struct TaskListView: View {
    @ObservedObject var categoryViewModel: CategoryViewModel
    var category: Category
    
    @State private var completionStatus = CompletionStatus.all
    
    @State private var showUpdate = false
    
    var filteredTasks: [Task] {
        var ts = [Task]()
        if completionStatus == .all {
            for i in 0..<categoryViewModel.tasks.count {
                if categoryViewModel.tasks[i].parentCategoryId == category.id {
                    ts.append(categoryViewModel.tasks[i])
                }
            }
        } else if completionStatus == .incomplete {
            for i in 0..<categoryViewModel.tasks.count {
                if categoryViewModel.tasks[i].parentCategoryId == category.id && categoryViewModel.tasks[i].completed == false {
                    ts.append(categoryViewModel.tasks[i])
                }
            }
        } else if completionStatus == .complete {
            for i in 0..<categoryViewModel.tasks.count {
                if categoryViewModel.tasks[i].parentCategoryId == category.id && categoryViewModel.tasks[i].completed == true {
                    ts.append(categoryViewModel.tasks[i])
                }
            }
        }
        return ts
    }
    
    var updateTask_VM = UpdateTaskViewModel.createNew_VM()
    
    var body: some View {
        VStack {
            Picker("Completion Status", selection: $completionStatus) {
                ForEach(CompletionStatus.allCases, id: \.self) { status in
                    Text(status.rawValue.capitalized)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding()
            
            List {
                ForEach(filteredTasks) { task in
                    TaskCellView(taskCell_VM: TaskCellViewModel(task: task))
                        .contextMenu{
                            Button(action: {
                                updateTask_VM.task.title = task.title
                                updateTask_VM.task.id = task.id
                                updateTask_VM.task.priority = task.priority
                                updateTask_VM.task.completed = task.completed
                                updateTask_VM.task.parentCategoryId = task.parentCategoryId
                                updateTask_VM.originalParentCategoryTitle = category.title // goes in the HEADER of the View
                                
                                showUpdate = true
                            }) {
                                Text("Edit Task")
                                Image(systemName: "square.and.pencil")
                            }
                        }
                }
                .onDelete { indexSet in
                    let x = filteredTasks[indexSet.first!]
                    categoryViewModel.deleteTask(with: x.id)
                }
            }
        }
        .navigationBarTitle(category.title, displayMode: .inline)
        
        .navigationBarItems(trailing:
        
                                Menu {
                                    Button(action: {
                                        updateTask_VM.task.title = ""
                                        updateTask_VM.task.id = ""
                                        updateTask_VM.task.priority = .medium
                                        updateTask_VM.task.completed = false
                                        updateTask_VM.task.parentCategoryId = category.id
                                        updateTask_VM.originalParentCategoryTitle = category.title
                                        showUpdate = true
                                    }) {
                                        Label("Add Task", systemImage: "plus.circle")
                                    }
                                    
                                    Button(action: {

                                    }) {
                                        Label("Edit Category", systemImage: "pencil.circle")
                                    }
                                } label: {
                                    Image(systemName: "ellipsis.circle")
                                        .font(.title2)
                                }
                            
//                                HStack{
//                                    Button(action: {
//                                        updateTask_VM.task.title = ""
//                                        updateTask_VM.task.id = ""
//                                        updateTask_VM.task.priority = .medium
//                                        updateTask_VM.task.completed = false
//                                        updateTask_VM.task.parentCategoryId = category.id
//                                        updateTask_VM.originalParentCategoryTitle = category.title
//                                        showUpdate = true
//                                    }, label: { Image(systemName: "ellipsis.circle") })
//
//                                    Button(action: {
//                                        updateTask_VM.task.title = ""
//                                        updateTask_VM.task.id = ""
//                                        updateTask_VM.task.priority = .medium
//                                        updateTask_VM.task.completed = false
//                                        updateTask_VM.task.parentCategoryId = category.id
//                                        updateTask_VM.originalParentCategoryTitle = category.title
//                                        showUpdate = true
//                                    }, label: { Image(systemName: "plus.circle") })
//                                }
        )
        
        .sheet(isPresented: $showUpdate) {
            UpdateTaskView(categoryViewModel: categoryViewModel, updateTask_VM: updateTask_VM)
        }
    }
}
