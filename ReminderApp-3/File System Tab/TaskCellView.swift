//
//  TaskCellView.swift
//  ReminderApp-3
//
//  Created by ap_test on 10/6/20.
//

import SwiftUI

struct TaskCellView: View {
    @ObservedObject var taskCell_VM: TaskCellViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                taskCell_VM.taskPriorityIcon.foregroundColor(taskCell_VM.taskPriorityIconColor)
                Text(taskCell_VM.task.title)
                Spacer()
                Image(systemName: taskCell_VM.completionStateIconName)
            }
//            Text(taskCell_VM.task.id)
//                .font(.caption)
//            Text(taskCell_VM.task.parentCategoryId)
//                .font(.caption)
        }
    }
    
}
