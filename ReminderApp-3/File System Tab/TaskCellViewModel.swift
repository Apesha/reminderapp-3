//
//  TaskCellViewModel.swift
//  ReminderApp-3
//
//  Created by ap_test on 10/6/20.
//

import SwiftUI
import Combine

class TaskCellViewModel: ObservableObject {
    @Published var task: Task
    
    @Published var taskPriorityIcon = Image(systemName: "circle")
    @Published var taskPriorityIconColor = Color.red
    @Published var completionStateIconName = ""

    private var cancellables = Set<AnyCancellable>()

    init(task: Task) {
        self.task = task
        
        $task
            .map {
                switch $0.priority {
                case .high:
                    return Image(systemName: "h.circle.fill")
                case .medium:
                    return Image(systemName: "m.circle.fill")
                case .low:
                    return Image(systemName: "l.circle.fill")
                }
            }
            .assign(to: \.taskPriorityIcon, on: self)
            .store(in: &cancellables)
        
        $task
            .map {
                switch $0.priority {
                case .high:
                    return Color.red
                case .medium:
                    return Color.purple
                case .low:
                    return Color.green
                }
            }
            .assign(to: \.taskPriorityIconColor, on: self)
            .store(in: &cancellables)
        
        $task
            .map {
                $0.completed ? "checkmark.circle.fill" : "circle"
            }
            .assign(to: \.completionStateIconName, on: self)
            .store(in: &cancellables)
    }
}
