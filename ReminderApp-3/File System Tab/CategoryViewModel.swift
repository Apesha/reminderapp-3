//
//  CategoryViewModel.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/28/20.
//

import SwiftUI

class CategoryViewModel: ObservableObject {
    @Published var categories = [Category]()
    @Published var tasks = [Task]()

    init() {
        categories = FileHelper.loadCategories()
        tasks = FileHelper.loadTasks()
    }
    
    func addCategory(newCategoryTitle: String) {
        let newCategory = Category(title: newCategoryTitle, displayOrder: Int(Date.timeIntervalSinceReferenceDate))
        categories.append(newCategory)
        FileHelper.saveCategories(categories)
    }
    
    func updateCategory(categoryId: String, categoryTitle: String) {
        if let index = categories.firstIndex(where: {$0.id == categoryId}) {
            categories[index].title = categoryTitle
        }
        FileHelper.saveCategories(categories)
    }
    
    func deleteCategory(atOffsets: IndexSet, completion: @escaping (String) -> Void) {
        var str = ""
        atOffsets.forEach { x in
            let categoryToRemove = categories[x]
            let numberOfTasks = FileHelper.countTasks(in: categoryToRemove)
            if numberOfTasks == 0 {
                str = "Removed category \(categoryToRemove.title)"
                categories.remove(atOffsets: atOffsets)
                FileHelper.saveCategories(categories)
            } else {
                str = "Category \(categoryToRemove.title) containt Tasks"
            }
        }
        
        completion(str)
    }
    
    func addTask(newTaskTitle: String, newTaskPriority: TaskPriority, parentCategoryId: String, completed: Bool) {
        let newTask = Task(title: newTaskTitle, priority: newTaskPriority, completed: completed, parentCategoryId: parentCategoryId)
        tasks.append(newTask)
        FileHelper.saveTasks(tasks)
    }
    
    func updateTask(taskId: String, taskTitle: String, taskPriority: TaskPriority, parentCategoryId: String, completed: Bool) {
        if let index = tasks.firstIndex(where: {$0.id == taskId}) {
            tasks[index].title = taskTitle
            tasks[index].priority = taskPriority
            tasks[index].parentCategoryId = parentCategoryId
            tasks[index].completed = completed
        }
        FileHelper.saveTasks(tasks)
    }
    
    func deleteTask(with id: String) {
        
        print(id)
        
        var index = 0
        for i in 0..<tasks.count {
            if tasks[i].id == id {
                index = i
                print(tasks[i].id)
            }
        }
        
        tasks.remove(at: index)
        FileHelper.saveTasks(tasks)
    }
}
