//
//  Models.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/29/20.
//

import Foundation

struct Task: Identifiable, Codable {
    var id = UUID().uuidString
    var title: String
    var priority: TaskPriority
    var completed: Bool
    var parentCategoryId: String
}

enum TaskPriority: String, Codable, CaseIterable {
    case high, medium, low
}

struct Category: Identifiable, Codable {
    var id = UUID().uuidString
    var title: String
    var displayOrder: Int
}

enum CompletionStatus: String, CaseIterable, Identifiable {
    case complete
    case incomplete
    case all

    var id: String { self.rawValue }
}
