//
//  UpdateCategoryView.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/28/20.
//

import SwiftUI

struct UpdateCategoryView: View {
    @Environment(\.presentationMode) var presentationMode
    var categoryViewModel: CategoryViewModel
    
    @Binding var categoryTitle: String
    @Binding var categoryId: String
    
    @State private var screenTitle = ""
    @State private var buttonLabel = ""

    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Enter new title", text: $categoryTitle)
                }
                
                Section {
                    Button {
                        if categoryTitle.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            if categoryId == "" {
                                categoryViewModel.addCategory(newCategoryTitle: categoryTitle)
                            } else {
                                categoryViewModel.updateCategory(categoryId: categoryId, categoryTitle: categoryTitle)
                            }
                            presentationMode.wrappedValue.dismiss()
                        }
                    } label: {
                        HStack {
                            Spacer()
                            Text(buttonLabel)
                            Spacer()
                        }
                    }
                }
                .disabled(categoryTitle.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)

            }
            .navigationBarTitle(screenTitle, displayMode: .inline)
            
            .navigationBarItems(trailing: Button(action: {
                presentationMode.wrappedValue.dismiss()
            }, label: {Image(systemName: "xmark.circle.fill")} ) )
        }
        .onAppear {
            if categoryId != "" {
                screenTitle = "Edit Category"
                buttonLabel = "Update"
            } else {
                screenTitle = "New Category"
                buttonLabel = "Save"
            }
        }
    }
}
