//
//  TempTaskView.swift
//  ReminderApp-3
//
//  Created by ap_test on 10/4/20.
//

import SwiftUI

struct UpdateTaskView: View {
    @Environment(\.presentationMode) var presentationMode
    var categoryViewModel: CategoryViewModel
    @ObservedObject var updateTask_VM: UpdateTaskViewModel
    
    @State private var screenTitle = ""
    @State private var buttonLabel = ""
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Name of task")) {
                    TextField("Task Name", text: $updateTask_VM.task.title)
                }
                
                Section(header: Text("Priority")) {
                    Picker("Priority", selection: $updateTask_VM.task.priority) {
                        ForEach(TaskPriority.allCases, id: \.self) { priority in
                            Text("\(priority.rawValue.capitalized)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("Current: \(updateTask_VM.originalParentCategoryTitle)")) {
                    Picker("Parent Category", selection: $updateTask_VM.task.parentCategoryId) {
                        ForEach(categoryViewModel.categories) { category in
                            Text("\(category.title)")
                        }
                    }
                }
                
                Section(header: Text("Status")) {
                    HStack {
                        Toggle(isOn: $updateTask_VM.task.completed) {
                            Text("Completed?")
                        }
                    }
                }
                
                Section {
                    Button {
                        if updateTask_VM.task.title.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            if updateTask_VM.task.id == "" {
                                categoryViewModel.addTask(newTaskTitle: updateTask_VM.task.title,
                                                          newTaskPriority: updateTask_VM.task.priority,
                                                          parentCategoryId: updateTask_VM.task.parentCategoryId,
                                                          completed: updateTask_VM.task.completed)
                            } else {
                                categoryViewModel.updateTask(taskId: updateTask_VM.task.id,
                                                             taskTitle: updateTask_VM.task.title,
                                                             taskPriority: updateTask_VM.task.priority,
                                                             parentCategoryId: updateTask_VM.task.parentCategoryId,
                                                             completed: updateTask_VM.task.completed)
                            }
                            presentationMode.wrappedValue.dismiss()
                        }
                    } label: {
                        HStack {
                            Spacer()
                            Text(buttonLabel)
                            Spacer()
                        }
                    }
                }
                .disabled(!updateTask_VM.isValidNewTask)
            }
            .navigationBarTitle(screenTitle, displayMode: .inline)
            
            .navigationBarItems(trailing: Button(action: {
                presentationMode.wrappedValue.dismiss()
            }, label: { Image(systemName: "xmark.circle.fill") }))
        }
        .onAppear {
            if updateTask_VM.task.id == "" {
                screenTitle = "New Task"
                buttonLabel = "Save"
            } else {
                screenTitle = "Edit Task"
                buttonLabel = "Update"
            }
        }
    }
}
