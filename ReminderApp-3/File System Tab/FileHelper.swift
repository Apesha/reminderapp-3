//
//  FileHelper.swift
//  RemindersApp-2
//
//  Created by ap_test on 9/26/20.
//

import Foundation

class FileHelper {
    
    static let categoriesFileName = "AllCategories"
    static let tasksFileName = "AllTasks"

    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    static func saveCategories(_ categories: [Category]) {
        do {
            let filename = FileHelper.getDocumentsDirectory().appendingPathComponent(Self.categoriesFileName)
            let data = try JSONEncoder().encode(categories)
            try data.write(to: filename, options: [.atomic])
        } catch {
            print("Unable to save categories")
        }
    }
    
    static func loadCategories() -> [Category] {
        let filename = FileHelper.getDocumentsDirectory().appendingPathComponent(Self.categoriesFileName)

        var t = [Category]()
        do {
            let data = try Data(contentsOf: filename)
            t = try JSONDecoder().decode([Category].self, from: data)
        } catch {
            print("Unable to load categories.")
        }
        return t
    }
    
    static func countTasks(in category: Category) -> Int {
        let filename = FileHelper.getDocumentsDirectory().appendingPathComponent(Self.tasksFileName)
        var t = [Task]()
        do {
            let data = try Data(contentsOf: filename)
            t = try JSONDecoder().decode([Task].self, from: data).filter {$0.parentCategoryId == category.id }
        } catch {
            print("Unable to count tasks")
        }
        return t.count
    }
    
    static func saveTasks(_ tasks: [Task]) {
        do {
            let filename = FileHelper.getDocumentsDirectory().appendingPathComponent(Self.tasksFileName)
            let data = try JSONEncoder().encode(tasks)
            try data.write(to: filename, options: [.atomic])
            print("Saving tasks array: \(tasks.count)")
        } catch {
            print("Unable to save tasks")
        }
    }
    
    static func loadTasks() -> [Task] {
        let filename = FileHelper.getDocumentsDirectory().appendingPathComponent(Self.tasksFileName)

        var t = [Task]()
        do {
            let data = try Data(contentsOf: filename)
            t = try JSONDecoder().decode([Task].self, from: data)
        } catch {
            print("Unable to load tasks.")
        }
        return t
    }
}
