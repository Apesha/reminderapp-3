//
//  CategoryView.swift
//  ReminderApp-3
//
//  Created by ap_test on 9/27/20.
//

import SwiftUI

struct CategoryListView: View {
    @StateObject var categoryViewModel = CategoryViewModel()
    
    @State private var showUpdateScreen = false
    @State private var showAlertOnDelete = false
    
    @State private var alertMessage = ""
    
    @State private var placeholderTitle = ""
    @State private var placeholderId = ""
    
    var body: some View {
        NavigationView {
            List {
                ForEach(categoryViewModel.categories) { category in
                    
                    NavigationLink (
                        destination: TaskListView(categoryViewModel: categoryViewModel,
                                                  category: category))
                    {
                        VStack(alignment: .leading) {
                            Text("[\(FileHelper.countTasks(in: category))] \(category.title)")
                            Text(category.id)
                                .font(.caption)
                        }
                        
                        .contextMenu {
                            Button(action: {
                                placeholderTitle = category.title
                                placeholderId = category.id
                                showUpdateScreen = true
                            }) {
                                Text("Edit Name")
                                Image(systemName: "square.and.pencil")
                            }
                        }
                    }
                }
                .onMove(perform: move)
                
                .onDelete { indexSet in
                    categoryViewModel.deleteCategory(atOffsets: indexSet) { result in
                        alertMessage = result
                        showAlertOnDelete = true
                    }
                }
            }
            .navigationTitle("Categories")
            
            .navigationBarItems(
                leading: EditButton(),
                trailing: Button( action: {
                    placeholderTitle = ""
                    placeholderId = ""
                    showUpdateScreen = true
                },
                label: {
                    Image(systemName:"plus.circle.fill")
                    .font(.title2)
                } ) )
            
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .sheet(isPresented: $showUpdateScreen) {
            UpdateCategoryView(categoryViewModel: categoryViewModel, categoryTitle: $placeholderTitle, categoryId: $placeholderId)
        }
        .alert(isPresented: $showAlertOnDelete) {
            Alert(title: Text("Attention"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
        }
    }
    
    func move(from source: IndexSet, to destination: Int) {
        categoryViewModel.categories.move(fromOffsets: source, toOffset: destination)
        FileHelper.saveCategories(categoryViewModel.categories)
    }
    
}
