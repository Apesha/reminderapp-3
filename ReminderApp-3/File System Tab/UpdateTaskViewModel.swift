//
//  UpdateTaskViewModel.swift
//  ReminderApp-3
//
//  Created by ap_test on 10/3/20.
//

import SwiftUI
import Combine

class UpdateTaskViewModel: ObservableObject {
    
    @Published var task: Task // because of @Published Category name changes when we make a new selection in the selector

    var originalParentCategoryTitle = ""

    var isValidNewTask: Bool {
        if task.title.isEmpty || task.parentCategoryId.isEmpty {
            return false
        }
        return true
    }
    
    static func createNew_VM() -> UpdateTaskViewModel {
        UpdateTaskViewModel(task: Task(id: "", title: "", priority: .medium, completed: false, parentCategoryId: ""))
    }
    
    private var cancellables = Set<AnyCancellable>()

    init(task: Task) {
        self.task = task
    }
}
